# Mondragon University - Software & System Engineering

[eu](README.md) | [es](README.es.md) | [en](README.en.md)

This is the git repository of the [*Software and Systems Engineering* research group](https://www.mondragon.edu/en/research-transfer/engineering-technology/research-and-transfer-groups/-/mu-inv-mapping/grupo/ingenieria-del-sw-y-sistemas) at Mondragon University.

All repositories:

- [GitHub](https://github.com/mu-sse)
- [GitLab](https://gitlab.com/mu-sse)