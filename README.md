# Mondragon Unibertsitatea - Software eta Sistemen Ingeniaritza

[eu](README.md) | [es](README.es.md) | [en](README.en.md)

Hau Mondragon Unibertsitateako [*Software eta Sistemen Ingeniaritza* ikerketa taldeko](https://www.mondragon.edu/eu/ikerketa-transferentzia/ingeniaritza-teknologia/ikerketa-transferentzia-taldeak/-/mu-inv-mapping/grupo/ingenieria-del-sw-y-sistemas) git biltegia da.

Ordezko biltegiak:

- [GitHub](https://github.com/mu-sse)
- [GitLab](https://gitlab.com/mu-sse)